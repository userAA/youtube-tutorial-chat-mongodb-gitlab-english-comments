gitlab page https://gitlab.com/userAA/youtube_tutorial_chat-gitlab-english-comments.git
gitlab comment youtube_tutorial_chat-gitlab-english-comments

project youtube_tutorial_chat (a fairly full-fledged chat)
technologies used on the frontend:
    @navjobs/upload,
    antd,
    axios,
    classnames,
    date-fns,
    emoji-mart,
    formik,
    identity-obj-proxy,
    lodash,
    nodemon,
    prop-types,
    react,
    react-dom,
    react-redux,
    react-router,
    react-router-dom,
    react-scripts,
    react-string-replace,
    redux,
    redux-thunk,
    sass,
    socket.io-client,
    tinycolor2;

//I use test data like this
email      test@test.ru
fullname   user1
password   qwerty123
password1  qwerty123

email      test1@test.ru
fullname   user2
password   qwerty1234
password1  qwerty1234

used technologies on the backend:
    @types/bcrypt,
    @types/dotenv,
    @types/express,
    @types/express-validator,
    @types/jsonwebtoken,
    @types/lodash,
    @types/mongoose,
    @types/multer,
    @types/node,
    @types/passport,
    @types/socket.io,
    @types/validator,
    bcrypt,
    body-parser,
    cloudinary,
    date-fns,
    dotenv,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer,
    nodemon,
    socket.io,
    ts-node,
    typescript,
    validator;

There is registration and authorization of user. 
At first moment is registered users. Then one from them is being authorized and with him is being created dialog, for which is being selected
any non authorized user.
Dialog is taking place only with two users. One can to create any quantity of users. Dialogues, respectively, too.
One can according to name of partner to find current dialog.
One can to switch according to which dialog between all available.
In every dialog one can except standart text messages, send friend-friend the images, sound messages and text messages with smiles.
Messages have a read status in the form of a picture of two checkmarks. It is placed after the partner answered them.
Otherwise, the messages have the status reading from one checkmarks. 
In every dialog from all mesages from author of dialogs indicates the time of them creating.
Any message in every dialog one can to remove.
If partner watches user messages and answeres on them, that he is too in status Online. 

