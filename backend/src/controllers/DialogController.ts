import express from "express";
import socket from "socket.io";
import {DialogModel, MessageModel} from "../models";

class DialogController 
{
    io: socket.Server;
    constructor(io: socket.Server) 
    {
        this.io = io;
    }

    //outputing of all dialogs function
    index = (req: express.Request, res: express.Response) => 
    { 
        //userId we receiving from token
        const userId = req.user._id;
   
        DialogModel.find().or([{ author: userId }, { partner: userId }])
            //we receiving full information about user author
            //we receiving full information about user partner
            .populate(["author","partner"])
            //we receiving full information about message lastMessge
            .populate({
                path: "lastMessage",
                //we receiving full information about user which wrote message lastMessage
                populate: {
                    path: "user"
                }
            })
            .exec(function(err,dialogs) 
            {
                if (err) 
                {
                    return res.status(404).json(
                    {
                        message: "Dialogs not found"
                    })
                }

                //entering obtained dialogs
                return res.json(dialogs);
            })
    }

    //the function of creating dialogs
    create = (req: express.Request, res: express.Response): void => 
    {
        const postData = 
        {
            //author of dialog is being taken from token
            author: req.user._id,
            //partner of dialog is being taken from received data
            partner: req.body.partner
        }

        //we creating dialog in database of dialogs
        const dialog = new DialogModel(postData);

        dialog.save().then((dialogObj: any) => 
        {
            //we creating message from user(author) to partner
            const message = new MessageModel(
            {
                text: req.body.text,
                user: req.user._id,
                dialog: dialogObj._id,
            })

            //we fixing formed message in database of messages
            message.save().then(() => 
            {
                dialogObj.lastMessage = message._id;
                dialogObj.save().then(() =>
                {
                    res.json(dialogObj);
                    //the signal according to socket about creating of new dialog
                    this.io.emit("SERVER:DIALOG_CREATED",{});
                });
            })
            .catch(reason => 
            {
                res.json(reason);
            })
        })
        .catch(err => 
        {
            res.json
            ({
                status: "error",
                message: err
            });
        })
    }
}

export default DialogController;