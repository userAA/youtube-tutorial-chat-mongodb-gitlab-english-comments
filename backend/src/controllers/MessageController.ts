import express from "express";
import socket from "socket.io";

import {MessageModel, DialogModel} from "../models";
import { IMessage } from "../models/Message";

class MessageController 
{
    io: socket.Server;

    constructor(io: socket.Server) 
    {
        this.io = io;
    }

    //the function of readed messages fixing 
    updateReadStatus = (res: express.Response, userId: string, dialogId: string): void => 
    {
        MessageModel.updateMany({dialog: dialogId, user: {$ne: userId}}, {$set: {readed: true}}, (err: any): void => 
        {
            if (err) 
            {
                res.status(500).json({status: "error", message: err});
            } 
        })
    }

    //the function of receiving full information about all messages in dialog 
    index = (req: express.Request, res: express.Response): void => 
    {
        //we obtaining the ID of dialog, information about all messages of which will collect
        const dialogId: string = req.query.dialog as string;

        MessageModel.find({dialog: dialogId}).populate(["dialog", "user","attachments"]).exec(function(err,messages) 
        {    
            if (err) 
            {    
                return res.status(404).json(
                {
                    status: "error",
                    message: "Messages not found"
                })
            }
            return res.json(messages);
        })   
    }

    //the function of creating new messages in dialog
    create = (req: express.Request, res: express.Response) => 
    {
        //ID user, which sent new message
        const userId: string = req.user._id;

        //data about new message
        const postData = 
        {
            text: req.body.text,
            dialog: req.body.dialog_id,
            attachments: req.body.attachments,
            user: userId,
        };

        const message = new MessageModel(postData);

        //we fixing messages read by partner
        this.updateReadStatus(res, userId, req.body.dialog_id);

        //we saving new message into database of messages
        message.save().then((obj: IMessage) => 
        {
            obj.populate("dialog user attachments", (err: any, message: IMessage) => 
            {
                if (err) 
                {
                    return res.status(500).json(
                    {
                        status: "error",
                        message: err
                    })
                }

                //data base of dialogs changes in dialog with ID postData.dialog 
                //appeared new last message with ID message._id
                DialogModel.findOneAndUpdate({_id: postData.dialog},{lastMessage: message._id},{upsert: true},
                    function(err) 
                    {
                        if (err) 
                        {
                            return res.status(500).json(
                            {
                                status: "error",
                                message: err
                            });
                        }
                    }
                )
                res.json(message);
                //socket-server, talking about that new message is taken
                this.io.emit("SERVER:NEW_MESSAGE",message);
            })
        })
        .catch(reason => 
        {
            res.json(reason);
        })
    }

    //the function of removing selected message in dialog
    delete = (req: express.Request, res: express.Response): void => 
    {
        //ID of removing message
        const messageDeleteId: string = req.query.id as string;

        MessageModel.findById(messageDeleteId, (err, message: any) => 
        {
            if (err || !message) 
            {
                return res.status(404).json
                ({
                    status: "error",
                    message: "Message not found",
                });
            }
    
            const dialogId = message.dialog;

            //we removing selected message
            message.remove();

            //we updating last message (it may change)
            MessageModel.find({dialog: dialogId}).sort('createdAt').exec((err, messages) => 
            {
                if (err) 
                {
                    res.status(500).json
                    ({
                        status: "error",
                        message: err,
                    });
                }
                
                //in database of dialogs we changing the ID of last message lastMessage in dialog with ID dialogId
                DialogModel.findById(dialogId, (err, dialog) => 
                {
                    if (err) 
                    {
                        res.status(500).json
                        ({
                            status: "error",
                            message: err,
                        });
                    }
    
                    if (!dialog) 
                    {
                        return res.status(404).json
                        ({
                            status: "not found",
                            message: err,
                        });
                    }
                  
                    //we changing last massage in dialog if there is it 
                    if (messages.length != 0)
                    {
                        dialog.lastMessage = ((messages[messages.length-1] as IMessage)._id).toString(); 
                    }
                    else
                    {
                        dialog.lastMessage = messageDeleteId;    
                    }
                    dialog.save();

                    this.io.emit("SERVER:CHANGE_LAST_MESSAGE",{});
                });
            })
  
            return res.json
            ({
                status: "success",
                message: "Message deleted",
            });
        });
    };
}

export default MessageController;