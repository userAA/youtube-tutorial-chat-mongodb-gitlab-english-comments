import express from "express";

import cloudinary from "../core/cloudinary";
import {UploadFileModel} from "../models";

class UploadController {

    //the function of removing files from database of files
    deleteFile = (res: express.Response, fileId: string): void => 
    {
        UploadFileModel.deleteOne({_id: fileId}, function(err: any)
        {
            if (err) {
                return res.status(500).json({
                    status: "error",
                    message: err
                })
            }
            res.json({
                status: "success"
            })
        })
    }

    //function of loading files into database according to files
    create = (req: express.Request, res: express.Response):void => 
    {
        //user ID, which loads file
        const userId = req.user._id;
        
        //information about uploading file
        const file: any = req.file;

        cloudinary.v2.uploader.upload_stream({resource_type: "auto"}, (error: any, result: any) => 
        {
            if (error) 
            {
                throw new Error(error);
            }

            const fileData = 
            {
                filename: result.original_filename,
                size: result.bytes,
                ext: result.format,
                url: result.url,
                user: userId
            };
        
            //we loading information about new file into database
            const uploadFile = new UploadFileModel(fileData);
            uploadFile.save().then((fileObj: any) => 
            {
                res.json({
                    status: "success",
                    file: fileObj
                });
            })
            .catch((err: any) => 
            {
                res.json({
                    status: "error",
                    message: err
                });
            });
        })
        .end(file.buffer);
    };

    //the function of removing file from database according to files which upload for creating new message
    delete = (req: express.Request, res: express.Response): void => {
        //ID of removing file
        const fileId: string = req.query.uid as string;
        //we removing file from database of files
        this.deleteFile(res, fileId);
    }
}

export default UploadController;