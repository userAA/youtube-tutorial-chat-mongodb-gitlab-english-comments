import bodyParser from "body-parser";
import express from "express";
import socket from "socket.io";
import {updateLastSeen, checkAuth} from '../middlewares';
import {loginValidation, registerValidation} from '../utils/validations';

import multer from "../core/multer";

import { 
    UserCtrl, 
    DialogCtrl, 
    MessageCtrl, 
    UploadFileCtrl
} from "../controllers";

const createRoutes = (app: express.Express, io: socket.Server) => 
{
    const UserController = new UserCtrl();
    const DialogController = new DialogCtrl(io);
    const MessageController = new MessageCtrl(io);
    const UploadFileController = new UploadFileCtrl();

    app.use(bodyParser.json());
    
    //user authorization checking 
    app.use(checkAuth);
    
    //we changing looking time of messages by user
    app.use(updateLastSeen);
    
    //router of receiving information about authorized user
    app.get("/user/me", UserController.getMe);
    //router of user registration according to email, fullname and password
    app.post("/user/signup", registerValidation, UserController.create);
    //router of user authorization
    app.post("/user/signin",loginValidation, UserController.login);
    //router of finding users
    app.get("/user/find", UserController.findUsers);
    
    //router of outputing all dialogs
    app.get("/dialogs", DialogController.index);
    //router of creating dialogs
    app.post("/dialogs", DialogController.create);
    
    //router of receiving full information about all messages in dialog
    app.get("/messages", MessageController.index);
    //router of removing of selected message in dialog
    app.delete("/messages", MessageController.delete);
    //router of creating new message in dialog
    app.post("/messages", MessageController.create); 

    //router of uploading file
    app.post("/files", multer.single("file"), UploadFileController.create);
    //the router of removing file from database, which uploads for creating another message
    app.delete("/files", UploadFileController.delete);
}

export default createRoutes;