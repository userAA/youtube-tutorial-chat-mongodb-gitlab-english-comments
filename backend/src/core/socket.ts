
import socket from "socket.io";
import http from "http";

export default (http: http.Server) => 
{
    const io = socket(http);

    io.on("connection", function(socket: any) 
    {
        //we fixing detector of creating messages with client-application
        socket.on("DIALOGS:TYPING", (obj: any) => {
            socket.emit("DIALOGS:TYPING",obj);
        })
    });

    return io;
}