import mongoose, {Schema,Document} from "mongoose";
import {IMessage} from "./Message";
import {IUser} from "./User";


//interface of an element of a database part storing information about dialogs
export interface IDialog extends Document 
{
    //partner ID of  dialog, according to it one can to define information about itself partner via function populate
    partner: IUser | string;
    //author ID of dialog, according to it one can define information about itself author via function populate
    author: IUser | string;
    //last message ID of dialog from author to partner, according to it one can define itself message via function populate
    lastMessage: IMessage | string; 
}

const DialogSchema = new Schema
(
    {
        partner: {type: Schema.Types.ObjectId, ref: "User"},
        author:  {type: Schema.Types.ObjectId, ref: "User"},
        lastMessage: {type: Schema.Types.ObjectId, ref: "Message"}
    }, 
    {
        timestamps: true
    }
);   

const DialogModel = mongoose.model<IDialog>('dialog', DialogSchema);

export default DialogModel;