import mongoose, {Schema,Document} from "mongoose";
import {IDialog} from "./Dialog";

//interface of an element of a database part storing information about messages
export interface IMessage extends Document 
{
    //text of message
    text:  string;           
    //ID of the dialog to which the message belongs, according to it one can to define information about dialog via function populate
    dialog: IDialog | string; 
    //flag status talking about that whether has been read message by partner or no
    readed:  boolean;         
}

const MessageSchema = new Schema
(
    {
        text: {type: String, require: true},
        dialog: {type: Schema.Types.ObjectId, ref: "Dialog", require: true},
        //user ID which sent this message to partner, according to it one can define information about partner via function populate
        user: {type: Schema.Types.ObjectId, ref: "User", require: true}, 
        readed: {type: Boolean, default: false},
        //the list of ID files in messages, if there are in it, according to it one can define information about files via function populate 
        attachments: [{type: Schema.Types.ObjectId, ref: "UploadFile"}]
    }, 
    {
        timestamps: true
    }
);   

const MessageModel = mongoose.model<IMessage>('message', MessageSchema);

export default MessageModel;