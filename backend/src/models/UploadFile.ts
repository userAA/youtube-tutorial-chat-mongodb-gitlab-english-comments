import mongoose, {Schema,Document} from "mongoose";
export {IUser} from "./User";
export {IMessage} from "./Message";

//interface of the file storage database part element
export interface IUploadFile 
{
    //file name
    filename: string;
    //his size
    size: number;   
    //his extension  
    ext: string;    
    //his address in cloudinary  
    url: string,     
    //message ID to which belongss this file, according to it one can to define information about message via function populate 
    message: string;
    //user ID which sent this file to partner as a message or his part, 
    //according to it one can define information about itself user via function populate
    user: string; 
}

export type IUploadFileDocument = Document & IUploadFile;

const UploadFileSchema = new Schema
(
    {
        filename: String,
        size: Number,
        ext: String,
        url: String,
        message: {type: Schema.Types.ObjectId, ref: "Message", require: true},
        user: {type: Schema.Types.ObjectId, ref: "User", require: true}
    }, 
    {
        timestamps: true
    }
);   

const UploadFileModel = mongoose.model<IUploadFileDocument>('uploadFile', UploadFileSchema);

export default UploadFileModel;