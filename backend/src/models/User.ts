import mongoose, {Schema,Document} from "mongoose";
import {isEmail} from "validator";
import {generatePasswordHash} from "../utils";
import differenceInMinutes from "date-fns/difference_in_minutes";

//interface of an element of a database part storing user information
export interface IUser extends Document {
    //email of user
    email: string;  
    //full name of user      
    fullname: string;    
    //the encrypted password of user
    password: string;  
    //encrypted creation date 
    confirm_hash: string; 
    //the time of looking messages of partner or answer on these messages
    last_seen?: Date;     
}

const UserSchema = new Schema
(
    {
        email: 
        {
            type: String,
            require: 'Email address is required',
            validate: [isEmail, "Invalid email"],
            unique: true
        }, 
        fullname: 
        {
            type: String,
            required: 'Fullname is required'
        },
        password: 
        {
            type: String,
            required: 'Password is required'
        },
        confirm_hash: String,
        last_seen: 
        {
            type: Date,
            default: new Date()
        }
    }, 
    {
        timestamps: true
    }
);   

//we defining whether the partner is online or no
UserSchema.virtual("isOnline").get(function(this:any) 
    {
        return differenceInMinutes(new Date().toISOString(), this.last_seen) < 3;
    }
);

UserSchema.set("toJSON", {
    virtuals: true,
});

UserSchema.pre<IUser>("save", async function (next) 
{
    const user = this;

    if (!user.isModified("password")) 
    {
        return next();
    }

    //we encrypting password user
    user.password = await generatePasswordHash(user.password);
    //we encrypting time of last changing in data base
    user.confirm_hash = await generatePasswordHash(new Date().toString());
})  

const UserModel = mongoose.model<IUser>('user',UserSchema);

export default UserModel;