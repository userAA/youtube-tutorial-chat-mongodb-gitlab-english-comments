import React from 'react';
import classNames from "classnames";

import './Block.scss';

//frame for window authorization user or registration user
const Block = ({children}) => {
    return (
        <div className={classNames('block', classNames)}>{children}</div>
    );
}

export default Block;