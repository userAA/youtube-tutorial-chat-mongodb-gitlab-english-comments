import React from 'react';
import PropTypes from "prop-types";
import {Button as BaseButton} from 'antd';
import classNames from 'classnames';

import './Button.scss';

//big button in window of authorization user or in window of registration user
const Button = (props) => {
    return (
        <BaseButton 
           {...props} 
            className={classNames('button',props.className, {
                'button--large': props.size === 'large'
            })}
        />
    );
}

Button.propTypes = {
    className: PropTypes.string
}

export default Button;