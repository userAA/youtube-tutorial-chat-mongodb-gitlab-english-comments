import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Button, Input } from "antd";
import { UploadField } from "@navjobs/upload";
import { Picker } from "emoji-mart";

import { UploadFiles } from "../index";

import "./ChatInput.scss";

const { TextArea } = Input;

const ChatInput = props => {
    const {
        //sending text into mesage
        value,              
        //the function of defining of sending text into message
        setValue,           
        //flag of appearing the list of smiles
        emojiPickerVisible,
        //the controlling function by value of flag of list of smiles appearing 
        toggleEmojiPicker, 
        //we adding smile into message
        addEmoji,           
        //we sending message by clicking on key Enter with code 13
        handleSendMessage,  
        //we sending a message using the checkmark button
        sendMessage,        
        //the function of collection files for sending into message
        onSelectFiles,      
        //the list of sending files into message
        attachments,         
        //the function of deleting a file from being sent to a message
        removeAttachment,    
        //flag of sound recording
        isRecording,        
        //record begining
        onRecord,            
        //record broken 
        onHideRecording,    
        //uploader flag of sounding message 
        isLoading           
    } = props;

  return (
    <Fragment>
        <div className="chat-input">
            <div>
                <div className="chat-input__smile-btn">
                    <div className="chat-input__emoji-picker">
                        {/*Appear the list with smiles at emojiPickerVisible === true*/}
                        {emojiPickerVisible && (
                            <Picker onSelect={emojiTag => addEmoji(emojiTag)} set="apple" />
                        )}
                    </div>
                    {/*The button of appearing list with smiles*/}
                    <Button
                        onClick={toggleEmojiPicker}
                        type="link"
                        shape="circle"
                        icon="smile"
                    />
                </div>
                {isRecording ? (
                    <div className="chat-input__record-status">
                        {/* Record detector */}
                        <i className="chat-input__record-status-bubble"></i>
                        Recording...
                        {/*The button of broken recording */}
                        <Button
                            onClick={onHideRecording}
                            type="link"
                            shape="circle"
                            icon="close"
                            className="stop-recording"
                        />
                    </div>
                ) : (
                    //Field entering of message from user to partner
                    //the message is being sent to server with help clicking key Enter with code 13
                    <TextArea
                        onChange={e => setValue(e.target.value)}
                        onKeyUp={handleSendMessage}
                        size="large"
                        placeholder="Enter the text of the message…"
                        value={value}
                        autosize={{ minRows: 1, maxRows: 6 }}
                    />
                )
                }

                <div className="chat-input__actions">
                    {/*Uploading files */ }         
                    <UploadField
                        //the function of fixing selected files onSelectFiles
                        onFiles={onSelectFiles}
                        containerProps={{
                            className: "chat-input__actions-upload-btn"
                        }}
                        uploadProps={{
                            accept: ".jpg,.jpeg,.png,.gif,.bmp",
                            multiple: "multiple"
                        }}
                    >
                        {/*The button of appearing the list with files*/}
                        <Button type="link" shape="circle" icon="camera" />
                    </UploadField>
                    
                    {isLoading ? (
                        //the detector of uploading sound message or message in the form of files 
                        <Button type="link" shape="circle" icon="loading" />
                        ) : isRecording || value || attachments.length ? ( 
                            //the button of sending any message (sound, files, text) from user to partner 
                            <Button
                                onClick={sendMessage}
                                type="link"
                                shape="circle"
                                icon="check-circle"
                            />
                        ) : 
                        (
                            //the button of recording sound message from user to partner
                            <div className="chat-input__record-btn">
                                <Button
                                    onClick={onRecord}
                                    type="link"
                                    shape="circle"
                                    icon="audio"
                                />
                            </div>
                        )
                    }
                </div>
                
            </div>
            {attachments.length > 0 && (
                //file uploader
                <div className="chat-input__attachments">
                    <UploadFiles
                        //we sending the function of removing files
                        removeAttachment={removeAttachment}
                        //we sending selected files
                        attachments={attachments}
                    />
                </div>
            )}
        </div>
    </Fragment>
  );
};

ChatInput.propTypes = {
    className: PropTypes.string
};

export default ChatInput;