import React from 'react';
import classNames from "classnames";
import format from "date-fns/format";
import isToday from "date-fns/is_today";
import {Link} from "react-router-dom";

import {Avatar} from "..";

//we receiving the time of creating message
const getMessageTime = createdAt => {
    if (isToday(createdAt)) {
        return format(createdAt,'HH:mm');    
    } else {
        return format(createdAt, 'DD.MM.YYYY');
    }
}

//we checking whether authorized user has writen last message 
const renderLastMessage = (message, userId) => {
    let text = '';
    //we checking whether there is attached file to last message
    if (!message.text && message.attachments.length) {
        text = 'attached file'; 
    } else {
        text = message.text;
    }
    return `${message.user._id === userId ? 'You: ' : ''} ${text}`;
}

const DialogItem = ({
    //ID dialog in list
    _id,                
    //ID of authorized user
    userId,             
    //ID of current dialog
    currentDialogId,    
    //partner dialog
    partner,           
    //last message 
    lastMessage        
}) => {

    return (
        //Link with site сайтом /dialog/${_id} at clicking 
        <Link to={`/dialog/${_id}`}> 
            <div className={classNames("dialogs__item",{
                "dialogs__item--online": partner.isOnline,
                //whether there is dialog in list current
                "dialogs__item--selected": currentDialogId === _id 
                })}
            >
                {/*Dialog avatar according to name of partner*/}
                <div className="dialogs__item-avatar">
                    <Avatar user={partner}/>
                </div>
                <div className="dialogs__item-info">
                    <div className="dialogs__item-info-top">
                        {/*we fixing name of partner*/}
                        <b>{partner.fullname}</b>
                        {/*we fixing time of last message from author or partner*/}
                        <span>
                            {lastMessage != null && getMessageTime(lastMessage.createdAt)}
                        </span>
                    </div>
                    <div className="dialogs__item-info-bottom">
                        {/*we fixing last message (if there is it, so dialog may be empty) */ }
                        <p>
                            {lastMessage != null && 
                            (renderLastMessage(lastMessage, userId))}
                        </p>
                    </div>
                </div>
            </div>
        </Link>
    );
}

export default DialogItem;