import React from 'react';
import {Empty, Input} from "antd";
import orderBy from "lodash/orderBy";

import './Dialogs.scss';

import DialogItem from "../DialogItem";

const Dialogs = ({
    //the list of dialogs
    items,          
    //ID of authorized user
    userId,         
    //dialog partner search function
    onSearch,       
    //according to inputValue the name of partner dialog is being seeking 
    inputValue,     
    //ID of current dialog
    currentDialogId 
}) => {

    return (
        <div className="dialogs">
            {/*The window of searching dialogs*/}
            <div className="dialogs__search">
                <Input.Search 
                    placeholder="Search among contacts"
                    onChange={e => onSearch(e.target.value)}
                    value={inputValue}
                />                        
            </div>
            {
                //we showing the list of dialogs
                items.length ? (
                    orderBy(items, ["created_at"], ["desc"]).map(item => {
                        return (
                            <DialogItem
                                key={item._id}  
                                //ID of authorized user 
                                userId={userId} 
                                //dialog ID
                                currentDialogId={currentDialogId} 
                                {...item}
                            />
                        )
                    })
                ) : (
                    //there is no fialogs
                    <Empty 
                        image={Empty.PRESENTED_IMAGE_SIMPLE} 
                        description="Nothing found"
                    />
                )
            }
        </div>
    );
}

export default Dialogs;