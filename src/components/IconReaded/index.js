import React from 'react';
import PropTypes from "prop-types";
import readedSvg from "../../assets/img/readed.svg";
import noReadedSvg from "../../assets/img/noreaded.svg";

const IconReaded = ({
    //a flag indicating whether a message is coming from authorized user
    isMe,           
    //flag indicating whether the message from the authorized user has been read by the partner or not
    isReaded        
}) => {
    return (
        (isMe && (isReaded ? 
        (
            //we entering status read if message was read by partner (two checkmarks)
            <img 
                className="message__icon-readed" 
                src={readedSvg} 
                alt="Readed icon" 
            />
        ) : (
             //enter status no read if message was not read by partner (one checkmark)
            <img
                className="message__icon-readed"
                src={noReadedSvg}
                alt="No readed icon"
            />
        ))) || null
    )
};

IconReaded.propTypes = {
  isMe: PropTypes.bool,
  isReaded: PropTypes.bool
};

export default IconReaded;
