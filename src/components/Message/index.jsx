import React, {useState, useRef, useEffect} from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";
import {Popover, Button, Icon} from "antd";
import {Emoji} from "emoji-mart";
import reactStringReplace from "react-string-replace";

import {convertCurrentTime, isAudio} from "../../utils/helpers";

import waveSvg from "../../assets/img/wave.svg";
import playSvg from "../../assets/img/play.svg";
import pauseSvg from "../../assets/img/pause.svg";

import {Avatar, IconReaded, Time} from "..";

import './Message.scss';

//audio message output
const MessageAudio = ({
    //audio-source of sounding message
    audioSrc 
}) => {
    //audio-element of sounding message  
    const audioElem = useRef(null);                     
    //we defining flag of listening of sounding message                                               
    const [isPlaying, setIsPlaying] = useState(false); 
    //we defining the part of listening duration of sounding message
    const [progress, setProgress] = useState(0);      
    //we determining the current time how long the audio message has been listening to
    const [currentTime, setCurrentTime] = useState(0);

    //the control by soundig massage starting - pause
    const tooglePlay = () => {
        if (!isPlaying) {
            //we begining to listen sound message
            audioElem.current.play();
        } else {
            //we pause, stop listening to the audio message
            audioElem.current.pause();
        }
    };

    useEffect(() => {
        //the volume of sounding message
        audioElem.current.volume="0.1"; 
        //listen of sounding message
        audioElem.current.addEventListener(
            'playing', 
            () => {
                setIsPlaying(true);
            },
            false
        );
        //end of sounding message
        audioElem.current.addEventListener(
            'ended', 
            () => {
                setIsPlaying(false);
                setIsPlaying(false);
                setCurrentTime(0);
            },
            false
        );
        //sounding message is fixed on pause
        audioElem.current.addEventListener(
            'pause', 
            () => {
                setIsPlaying(false);
            },
            false
        );
        audioElem.current.addEventListener('timeupdate',() => {
            const duration = audioElem.current ? audioElem.current.duration : 0;
            //the time which passed according to sounding message
            setCurrentTime(audioElem.current.currentTime);
            //the scale of tracking sounding message
            setProgress((audioElem.current.currentTime/duration)*100);
        });
    },[]);

    return (
        <div className="message__audio">
            {/*The link between audio-source and audio-element of sounding message */}
            <audio ref={audioElem} src={audioSrc} preload="audio"/>
            <div className="message__audio-progress" style={{width: progress+'%'}}></div>
            <div className="message__audio-info">
                <div className="message__audio-btn">
                    <button onClick={tooglePlay}>
                        {isPlaying ? (
                        //sound message is being listening, one can to put it on pause
                        <img src={pauseSvg} alt="Pause svg"/>
                        ): (
                        //sound message is fixed on pause one can to continue listen it
                        <img src={playSvg} alt="Play svg"/>
                        )}
                    </button>
                </div>
                <div className="message__audio-wave">
                    <img src={waveSvg} alt="Wave svg"/>
                </div>
                <span className="message__audio-duration">
                    {/*time convert of listening sounding messages*/}
                    {convertCurrentTime(currentTime)}
                </span>
            </div>
        </div>
    )
}

const Message = ({
    //the flag of creating messages
    isTyping,          
    //isMe - flag, which detect about is there a message coming from user to partner or overwise
    isMe,              
    //the function of removing message
    onRemoveMessage,    
    //the function of indicating image in message
    setPreviewImage,    
    //authorized user
    user,               
    //text of message
    text,               
    //the time of creating message
    createdAt,          
    //status read
    readed,             
    //the files of messages
    attachments         
}) => {

    const renderAttachment = item => {
        if (item.ext !== "webm") {
            //we outputing file
            return (
                <div 
                    key={item._id}
                    //we highlighting the message on the entire window of the web page
                    onClick={() => setPreviewImage(item.url)}
                    className="message__attachments-item"
                >
                    <div className="message__attachments-item-overlay">
                        <Icon type="eye" style={{color: "white", fontSize: 18}}/>
                    </div>    
                    <img src={item.url} alt={item.filename}/>
                </div>
            )
        } else {
            //we outputing  sound message
            return (
                <MessageAudio key={item._id} audioSrc={item.url}/>
            )
        }
    }

    return (
        <div 
            className={classNames("message",{
                //message layout from authorized user
                'message--isme': isMe,                    
                //layout of message creating detector
                'message--is-typing': isTyping,           
                //layout of sounding message
                'message--is-audio': isAudio(attachments),
                //layout of message from one file 
                'message--image':                         
                    !isAudio(attachments) && 
                    attachments && 
                    attachments.length === 1 && 
                    !text,
            })}
        >
            <div className="message__content">
                {/*An icon that indicates whether an authorized user is writing a message or whether it has been read by the partner. */}
                <IconReaded isMe={isMe} isReaded={readed}/>
                {/*The window of removing message*/}
                <Popover
                    content={
                    <div>
                        <Button onClick={onRemoveMessage}>Remove message</Button>
                    </div>
                    }
                    //the event of removing message
                    trigger="click" 
                >
                    <div className="message__icon-actions">
                        <Button type="link" shape="circle" icon="ellipsis" />
                    </div>
                </Popover>
                {/*user avatar*/}
                <div className="message__avatar">
                    <Avatar user={user}/>
                </div>
                {/*Itself message*/}
                <div className="message__info">
                    {(text || isTyping) && (
                        <div className="message__bubble">
                            {text &&  (
                                //Output text message with possible smile
                                <p className="message__text">
                                    {reactStringReplace(text, /:(.+?):/g, (match, i) => (
                                        <Emoji key={i} emoji={match} set="apple" size={16} />
                                    ))}
                                </p>
                            )} 
                            {/*Output detector of creating message */}
                            {isTyping && (
                                <div className="message__typing"> 
                                    <span />
                                    <span />
                                    <span />    
                                </div>
                            )}
                        </div>
                    )}
                    
                    {/*Output of file message*/}
                    {attachments && (  
                        <div className="message__attachments">
                            {attachments.map((item,index) => renderAttachment(item))}
                        </div>
                    )}
                    
                    {/*Output the time of creating message */}
                    {createdAt && (
                        <span className="message__date">
                            <Time date={createdAt}/>
                        </span>
                    )}
                </div>
            </div>
        </div>
    );
}

Message.defaultProps = {
    user: {}
}

Message.propTypes = {
    avatar: PropTypes.string,
    text: PropTypes.string,
    date: PropTypes.string,
    user: PropTypes.object,
    attachments: PropTypes.array,
    isMe: PropTypes.bool,
    isReaded: PropTypes.bool,
    isTyping: PropTypes.bool,
    audio: PropTypes.string
}

export default Message;