import React from 'react';
import {Modal} from "antd";
import PropTypes from "prop-types";
import {Empty,Spin} from "antd";
import classNames from "classnames";

import Message from "../Message";

import "./Messages.scss";

//list messages
const Messages = ({
    //authorized user
    user,               
    blockRef, 
    //the list messages according to concrete dialog
    items,             
    //the function of removing selected message
    onRemoveMessage,   
    //the flag of showing detector of creating messages
    isLoading,          
    //the function of defining of selected image of some message
    setPreviewImage,    
    //selected image of some message, which it is shown in full screen
    previewImage,       
    //the list of messages rising at uploading of files for next message
    blockHeight,        
    //flag of detector of creating message
    isTyping,           
    //the partner of dialog messages
    partner             
}) => {

    return (
        //at uploading of files messages are rising on blockHeight
        <div style = {{height: `calc(100% - ${blockHeight}px)`}}>
            <div
                ref = {blockRef}
                className={classNames("messages",{"messages--loading":isLoading})}
            >
                {
                    isLoading  ? 
                    (
                        //Messages are being loaded
                        <Spin size="large" tip="Uploading messages..."/>
                    ) 
                    : items && !isLoading ? 
                    (
                        items.length > 0 ? 
                        (
                            //we uploading all messages if there are any
                            items.map(item => {
                                return (               
                                    //we uploading one message at the time
                                    <Message  
                                        key={item._id}
                                        //the flag of creating message
                                        isTyping={false}
                                        //isMe - flag, which signals whether the message is coming from the user to the interlocutor or vice versa
                                        isMe={user._id === item.user._id}
                                        //bind because that function onRemoveMessage in right part is function 
                                        //removeMessageById from file "../../redux/actions/messages"

                                        //the function of removing message
                                        onRemoveMessage={onRemoveMessage.bind(this,item)} 

                                        //the function of selected image in message
                                        setPreviewImage={setPreviewImage} 
                                        {...item} 
                                    />
                                )
                            })
                        )
                        : 
                        (
                            //there is no message in dialog
                            <Empty description="Dialog empty"/>
                        )
                    )
                    : 
                    (
                        //necessary to open dialog
                        <Empty description="Open dialog"/>
                    )
                }
                {
                    //Detector about, that message is being created
                    isTyping && 
                    (
                        <Message 
                            isTyping={isTyping} 
                            user={partner} 
                        />
                    )
                }
                {/*we opening modal window of selected file of message */}
                <Modal
                    visible={!!previewImage}
                    onCancel={() => setPreviewImage(null)}
                    footer={null}
                >
                    <img src={previewImage} style={{width: "100%"}} alt="Preview"/>
                </Modal>
            </div>
        </div>
    )
};

Messages.propTypes = {
  items: PropTypes.array
};

export default Messages;