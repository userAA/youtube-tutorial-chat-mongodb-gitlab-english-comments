import React from "react";
import { Button, Modal, Select, Input, Form } from "antd";
import {Dialogs} from "../../containers";

import "./Sidebar.scss";

const {Option} = Select;
const {TextArea} = Input;

const Sidebar = ({
    //authorized user
    user,              
    //the label of the search for the name of the partner
    inputValue,    
    //visibility of the modal dialog creation window    
    visible, 
    //message to the partner         
    messageText,       
    //flag for loading the names of all users corresponding to the inputValue label
    isLoading,         
    //the function of closing the modal dialog creation window
    onClose,      
    //the function of showing a modal dialog creation window    
    onShow,     
    //the function of searching for the partner by tag     
    onSearch,          
    //the function of changing the search label of the partners's name
    onChangeInput,     
    //the function of selecting the partners's ID
    onSelectUser,      
    //the function of adding a dialog
    onModalOk,         
    //the function of composing a message to the partner
    onChangeTextArea,  
    //ID partner
    selectedUserId,    
    //list of partners
    users              
}) => {
    //list of user names
    const options = users.map(user => {
        return (
            <Option key={user._id}>{user.fullname}</Option>
        )
    });

    return (
        <div className="chat__sidebar">
            <div className = "chat__sidebar-header">
                <div>
                    <Button  type="link"  shape="circle"  icon="team"/>
                    <span>List of dialogs</span>
                </div>   
                {/*The button for showing the dialog creation window*/}          
                <Button
                    onClick = {onShow}  
                    type="link"  
                    shape="circle"  
                    icon="form"
                />
            </div>
        
            {/*List of dialogs*/}
            <div className="chat__sidebar-dialogs">
                <Dialogs userId = {user && user._id} />
            </div>

            {/*dialog creation window*/}
            <Modal
                title="Create dialog"
                visible={visible}
                onOk={onModalOk}
                onCancel={onClose}
                footer={[
                    //we do not create dialog
                    <Button key="back" onClick={onClose}>
                        Close
                    </Button>,
                    //create dialog
                    <Button
                        disabled={!messageText}
                        key="submit"
                        type="primary"
                        loading={isLoading}
                        onClick={onModalOk}
                    >
                        Create
                    </Button>
                ]}
            >
                <Form className="add-dialog-form">
                    {/*Choosing an partner*/}
                    <Form.Item label="Enter the user name or E-Mail">
                        <Select
                            value={inputValue}
                            onSearch={onSearch}
                            onChange={onChangeInput}
                            onSelect={onSelectUser}
                            notFoundContent={null}
                            style={{width: `100%`}}
                            defaultActiveFirstOption={false}
                            showArrow={false}
                            filterOption={false}
                            placeholder="Enter the user name"
                            showSearch
                        >
                            {options}
                        </Select>
                    </Form.Item>
                    
                    {selectedUserId && (
                        //We enter the first message in the dialog from the authorized user to the partner
                        <Form.Item label="Enter text message">
                            <TextArea
                                autosize={{ minRows: 3, maxRows: 10 }}
                                onChange={onChangeTextArea}
                                value={messageText}
                            />
                        </Form.Item>
                    )}
                </Form>
            </Modal>
        </div>
    )
}

Sidebar.defaultProps = {
    users: []
};

export default Sidebar;