import React from 'react';
import PropTypes from "prop-types";
import classNames from "classnames";

import "./Status.scss";

//we showing status of partner, if he three minutes do not wathes message then there is no he in online 
const Status = ({
    //status of partner (does he or she read the messages sent to him or not)
    online, 
    //fullname of partner
    fullname  
}) => {
    return (
        <div className="chat__dialog-header">
            <div className="chat__dialog-header-center">
                <b className="chat__dialog-header-username">{fullname}</b>
                <div className="chat__dialog-header-status">
                    <span className={classNames("status",{"status--online": online})}>
                        {online ? "online" : "offline"}
                    </span>
                </div>
            </div>
        </div>
    )
};

Status.propTypes = {
    online: PropTypes.bool
};

export default Status;