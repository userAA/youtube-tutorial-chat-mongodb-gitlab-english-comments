import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import {filesApi} from "../utils/api";
import socket from "../core/socket"

import { ChatInput as ChatInputBase} from "../components"

import {messagesActions, attachmentsActions} from "../redux/actions";

const ChatInput = (props) => {
    const {
        //we receiving from ../../redux/reducers/dialogs with help connect
        dialogs: {currentDialogId},  
        //we receiving from  ../../redux/reducers/attachments with help connect
        attachments,                 
        //we receiving from ../../redux/actions/messagesActions 
        fetchSendMessage,            
        //we receiving from ../../redux/actions/attachmentsActions
        setAttachments,              
        //we receiving from ../../redux/actions/attachmentsActions
        removeAttachment            
    } = props;

    window.navigator.getUserMedia = (
        window.navigator.getUserMedia ||
        window.navigator.mozGetUserMedia ||
        window.navigator.msGetUserMedia ||
        window.navigator.webkitGetUserMedia
    )

    //definition of message text
    const [value, setValue] = useState('');                           
    //definition of recording sound flag 
    const [isRecording, setIsRecording] = useState('');               
    //setting the recorder
    const [mediaRecorder, setMediaRecorder] = useState(null);         
    //definition of flag visibility of lists with smiles
    const [emojiPickerVisible, setShowEmojiPicker] = useState(false); 
    //we defining flag of uploading sound message
    const [isLoading, setLoading] = useState(false);                  
    //we opening list of smiles
    const toggleEmojiPicker = () => {
        setShowEmojiPicker(!emojiPickerVisible);
    }

    //audio-message record
    const onRecord = () => {
        if (navigator.getUserMedia) {
            navigator.getUserMedia({audio: true},onRecording,onError)
        }
    }

    //the process of sound record
    const onRecording = (stream) => {
        //start up recorder
        const recorder = new MediaRecorder(stream);
        setMediaRecorder(recorder);
         
        recorder.start();

        //we begining sound recording
        recorder.onstart = () => {
            setIsRecording(true);
        }

        //we stoping sound recording
        recorder.onstop = function(e) {
            setIsRecording(false);
        }

        recorder.ondataavailable = e => {
            const file = new File([e.data],"audio.webm");
            setLoading(true)
            //we sending the sound file into database to server
            filesApi.upload(file).then(({data}) => {
                //at result we receiving sound message from data base on server
                sendAudio(data.file._id).then(() => {
                    setLoading(false);
                })
            })
        }
    }

    const onError = (err) => {
        console.log('The following error occured: ' + err)
    }

    const handleOutsideClick = (el, e) => {
        if (el && !el.contains(e.target)) {
            setShowEmojiPicker(false);
        }
    }

    //we adding smile in message value
    const addEmoji = ({colons}) => {
        setValue((value + ' ' + colons).trim());
    }

    //we receiving sound message
    const sendAudio = (audioId) => {
        //we making up request into server according to sound message
        return fetchSendMessage({
            text: null,
            dialogId: currentDialogId,
            attachments: [audioId]
        })
    }

    //sending any message
    const sendMessage = () => {
        if (isRecording) {
            //we stoping record of sound
            mediaRecorder.stop();        
        } else if (value || attachments.length>0) 
        {
            //sending a message to the server
            fetchSendMessage({
                //text message
                text: value,                                    
                //ID of message dialog
                dialogId: currentDialogId,                      
                //IDs of files in message
                attachments: attachments.map(file => file.uid), 
            });
            //we clearing text of message if he was in it
            setValue("")
            //we removing files of messages if they were in it
            setAttachments([])
        }
    }


    //we sending message into server 
    const handleSendMessage = (e) => {
        //this socket talk about that need to include detector of creating and sending message and it goes through the server - backend
        socket.emit('DIALOGS:TYPING', {});

        //the sending of message at clicking key enter with code 13
        if (e.keyCode === 13) {
            sendMessage();
        }
    }

    //we interrupting the sound recording and do not send it to the server
    const onHideRecording = () => {
        setIsRecording(false)
    }

    //we selecting files which sending to message
    const onSelectFiles = async files => { 
        
        let uploaded = attachments;

        for (let i=0; i < files.length; i++) {
            
            const file = files[i];
            const uid = Math.round(Math.random()*1000);
            uploaded = [
                ...uploaded,
                {
                    uid,
                    name: file.name,
                    status: "uploading"
                }
            ];

            let curAttachments = uploaded;

            //we fixing appearance of which new file
            uploaded =  await filesApi.upload(file).then(({data}) => {
                return (
                    curAttachments.map(item => {
                        if (item.uid === uid) {
                            return {
                                status: "done",
                                uid: data.file._id,
                                name: data.file.filename,
                                url: data.file.url
                            }
                        }
                        return item;  
                    })
                )
            })
        }
        setAttachments(uploaded);        
    }

    useEffect (() => {
        const el = document.querySelector('.chat-input__smile-btn');
        document.addEventListener("click",handleOutsideClick.bind(this,el));

        return () => {
            document.removeEventListener("click",handleOutsideClick.bind(this,el));
        }
    },[])

    //if dialog is not identificated than there is no the form of creating of messages
    if (!currentDialogId) {
        return null;
    }

    //we returning the form of creating messages 
    return (
        <ChatInputBase 
            //sending text into message
            value={value}                          
            //the function of determining the text to be sent to the message
            setValue={setValue}                    
            //the flag of appearance list of smiles
            emojiPickerVisible={emojiPickerVisible} 
            //the function of controlling the value of the flag for the appearance of a list of emoticons
            toggleEmojiPicker={toggleEmojiPicker}   
            //we adding smile into message
            addEmoji={addEmoji}                     
            //we sending message according to key Enter with code 13
            handleSendMessage={handleSendMessage}   
            //we sending message according to check mark
            sendMessage={sendMessage}               
            //the function of collecting files for sending in message
            onSelectFiles={onSelectFiles}          
            //the list of sending files in message
            attachments={attachments}               
            //the function of removing file from sending in message
            removeAttachment={removeAttachment}     
            //the flag of record sound
            isRecording={isRecording}               
            //begin record
            onRecord={onRecord}                     
            //record broken
            onHideRecording={onHideRecording}     
            //the flag of uploading sound message  
            isLoading={isLoading}                   
        />
    );
}

export default connect(
    ({
        //dialogs we receiving from ../../redux/reducers/dialogs
        dialogs,  
        //attachments we receiving from ../../redux/reducers/attachments
        attachments 
    }) => ({
        dialogs,                      
        //from attachments we finding attachments - attachments.items
        attachments: attachments.items 
    }), 
    {
        //from messagesActions we finding function fetchSendMessage
        ...messagesActions, 
        //from attachmentsActions we finding functions setAttachments and removeAttachments 
        ...attachmentsActions 
    }
)(ChatInput);