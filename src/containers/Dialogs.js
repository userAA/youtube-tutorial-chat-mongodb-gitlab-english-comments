import React, {useState, useEffect} from "react";
import {connect} from "react-redux";

import {dialogsActions} from "../redux/actions";
import socket from "../core/socket";

import {Dialogs as BaseDialogs} from "../components";

const Dialogs = (props) => {
    const {        
        //we receiving according to dialogs from ../../redux/reducers/dialogs with help connect
        currentDialogId,  
        //we receiving according to items from ../../redux/reducers/dialogs with help connect
        items,            
        //the function of downloading dialogs from the server is obtained from dialogsActions with help connect
        fetchDialogs,     
        //ID of authorized user
        userId 
    } = props;

    //according to this row is being seeked the name of author or partner of needed dialog
    const [inputValue, setValue] = useState('');

    //the array of dialogs in particular , when searching for a specific
    const [filtred, setFiltredItems] = useState(Array.from(items));

    //the function of searching specific dialog according to name of it author or partner 
    const onChangeInput = value => {
        setFiltredItems(items.filter(
            dialog => {
                return dialog.author.fullname.toLowerCase().indexOf(value.toLowerCase()) >=0 || 
                       dialog.partner.fullname.toLowerCase().indexOf(value.toLowerCase()) >=0
            }
        ));
        setValue(value);
    }

    useEffect(() => {
        if (!items.length) {
            //we downloading the dialogs from server if there are no them
            fetchDialogs();
        } else{
            //we fix the number of dialogs if there are they (it can change )
            setFiltredItems(items);
        }
        //this socket signals that a new dialog has been created
        socket.on('SERVER:DIALOG_CREATED', fetchDialogs);
        //this socket signals that new messages have been added to some of the dialogs
         socket.on('SERVER:NEW_MESSAGE', fetchDialogs);
        //at removing from dialog of last message, the last message in the dialog may change 
        socket.on('SERVER:CHANGE_LAST_MESSAGE',fetchDialogs);
        
        return () => {
            socket.removeListener('SERVER:DIALOG_CREATED', fetchDialogs);
            socket.removeListener('SERVER:NEW_MESSAGE', fetchDialogs);
            socket.removeListener('SERVER:CHANGE_LAST_MESSAGE', fetchDialogs);
        };
    },[fetchDialogs,items]);

    return (
        <BaseDialogs
            //ID of authorized user
            userId = {userId}                 
            //the list of dialogs
            items={filtred}       
            //the search of dialogs         
            onSearch={onChangeInput}          
            //the label of searching author or partner of dialog 
            inputValue={inputValue}           
            //ID of current dialog
            currentDialogId={currentDialogId} 
        />
    )
}

export default connect(
    ({
        //dialogs we receiving from ../../redux/reducers/dialogs.js
        dialogs 
    }) => { 
        return (
            dialogs
        )
    }, 
        //from dialogsActions we finding function fetchDialogs
        dialogsActions 
    )
(Dialogs);