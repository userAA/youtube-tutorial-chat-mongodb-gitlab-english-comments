import React, {useEffect, useState, useRef} from "react";
import {connect} from "react-redux";
import {Empty} from "antd";
import find from 'lodash/find';

import {messagesActions} from "../redux/actions";
import {Messages as BaseMessages} from "../components";
import socket from "../core/socket";

const Messages = ({
    //we receiving from ../../redux/reducers/dialogs with help connect
    currentDialog,  
    //we receiving from ../../redux/reducers/messages with help connect
    items,          
    //we receiving from ../../redux/reducers/messages with help connect
    isLoading,      
    //we receiving from ../../redux/reducers/user with help connect
    user,           
    //we receiving from ../../redux/reducers/attachments with help connect
    attachments,    
    //the function of receiving all messages from the server is obtained from messagesActions with help connect
    fetchMessages,
    //the function of adding message is obtained from messagesActions with help connect 
    addMessage,
    //the function of removing message is obtained from messagesActions with help connect
    removeMessageById 
}) => {
    //definition of selecting file in message
    const [previewImage, setPreviewImage] = useState(null); 
    //the definition of lifting height of lists messages at uploading file for next message
    const [blockHeight, setBlockHeight] = useState(135);
    //the definition of flag appearance of detector creating message 
    const [isTyping, setIsTyping] = useState(false);       
   
    const messagesRef = useRef(null); 

    useEffect(() => {
        let typingTimeoutId = null;
        
        const toggleIsTyping = () => {
            setIsTyping(true);
            clearInterval(typingTimeoutId);
            typingTimeoutId = setTimeout(() => {
                setIsTyping(false);
            }, 3000)
        }

        //when you enable this socket, this hook works
        socket.on("DIALOGS:TYPING", toggleIsTyping);
    },[])

    useEffect(() => {
        //the checking of uploading image
        if (attachments.length) {
            setBlockHeight(245);
        } else {
            setBlockHeight(135);
        }
        //if there is a number of uploaded images, this hook works
    },[attachments])

    useEffect(() => {
        if (currentDialog) {
            //current dialog currentDialog is updated
            //from this function we receiving all message in dialog with ID currentDialog._id
            fetchMessages(currentDialog._id);
        }

        //there is signal about that message is sent
        socket.on("SERVER:NEW_MESSAGE", addMessage);
        return () => {
            socket.removeListener("SERVER:NEW_MESSAGE",addMessage);
        }
        //the list of changing objects and called functions at which is being restarted this hook
    },[currentDialog,addMessage,fetchMessages]);
 
    useEffect(() => {
        if (currentDialog) messagesRef.current.scrollTo(0,99999);
        //the list of changing objects and called functions at which is being restart this hook
    },[items, isTyping, currentDialog]);

    //if current fialog is registered than we opening messages of this dialog
    return (currentDialog ? 
        <BaseMessages 
            //authorizes user
            user={user}                         
            blockRef={messagesRef} 
            //the list of messages according to specified dialog
            items={items}                        
            //the function of removing selected message
            onRemoveMessage={removeMessageById}  
            //the flag of showing detector of creating message
            isLoading={isLoading && !user}       
            //the function of defining selected image of some message
            setPreviewImage={setPreviewImage}  
            //the selected image of some message, which shows in full screen
            previewImage={previewImage}       
            //the message list goes up when uploading files for the next message
            blockHeight={blockHeight}          
            //the flag detector of creating message
            isTyping={isTyping}                  
            //the partner of messages dialog 
            partner={                            
                user._id !== currentDialog.partner._id ? currentDialog.author : currentDialog.partner
            }
        /> : 
        <Empty description="Open dialog"/>
    )
}

export default connect(
    ({
        //dialogs we receiving from ../../redux/reducers/dialogs.js
        dialogs,        
        //messages we receiving from ../../redux/reducers/messages.js 
        messages,       
        //user we receiving from ../../redux/reducers/user.js
        user,           
        //attachments we receiving from ../../redux/reducers/attachments.js
        attachments     
    }) => {
        return ({
            //we defining current dialog according to ID dialog
            currentDialog: find(dialogs.items, { _id: dialogs.currentDialogId }),
            //the list messsages of current dialog
            items: messages.items,
            //the detector of uploading messages
            isLoading: messages.isLoading,
            //the list of all new uploaded files
            attachments: attachments.items,
            //the data of authorized user
            user: user.data
        })
    }, 
    //from messagesActions is being found functions fetchMessages, addMessage and removeMessageById
    messagesActions 
)(Messages);