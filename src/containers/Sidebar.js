import React, {useState} from "react";
import {connect} from "react-redux";
import {userApi, dialogsApi} from "../utils/api";

import {Sidebar} from "../components";

const SidebarContainer = (props) => {
    const {
        //user - authorized user from ../../redux/reducers/user with help connect
        user 
    } = props;

    //we defining flag of opening modal window of creating dialog  
    const [visible, setVisible] = useState(false);                
    //we defining label of searching of name partner
    const [inputValue, setInputValue] = useState("");             
    //we defining text message to partner
    const [messageText, setMessageText] = useState("");     
    //we defining the list of partners    
    const [users, setUsers] = useState([]);     
    //we defining flag of uploading name of all users, corresponding label inputValue                 
    const [isLoading, setIsLoading] = useState(false);         
    //ID of selected partner
    const [selectedUserId, setSelectedUserId] = useState(false); 

    //we closing modal window of creating dialog
    const onClose = () => {
        setVisible(false)
    }

    //we showing modal window of creating dialog
    const onShow = () => {
        setVisible(true);
    }
    
    const onSearch = (value) => {
        setIsLoading(true);
        //we outputing the list of users corresponding to needed partner
        userApi.findUsers(value).then(({data}) => {
            setUsers(data);
            setIsLoading(false);
        }).catch(() => {
            setIsLoading(false);
        })
    }

    const onAddDialog = () => {
        //create dialog from user, ID of which is being defined according to token of user on server
        //according ID of partner selectedUserId and according to message from partner to user messageText
        dialogsApi
        .create({
            //partner
            partner: selectedUserId,  
            //the message to partner
            text: messageText         
        })
        .then(onClose)
        .catch(() => {
            setIsLoading(false);
        }) 
    }

    const handleChangeInput = (value) => {
        //value - ID of selected partner
        setInputValue(value);    
    }

    const onChangeTextArea = e => {
        //we defining the message from partner to user
        setMessageText(e.target.value);
    }

    const onSelectUser = (userId) => {
        //we fixing ID of selected partner
        setSelectedUserId(userId);
    };

    return (
        <Sidebar 
            //authorized user
            user={user}                         
            //the label searching of partner name
            inputValue={inputValue}             
            //the visibility of modal window of creating dialog
            visible={visible}                 
            //the message to partner
            messageText={messageText}  
            //the flag of uploading names of all users, corresponding label inputValue       
            isLoading={isLoading}             
            //the function of closing modal window of creating dialog
            onClose={onClose}       
            //the function of showing modal window of creating dialog        
            onShow={onShow}                   
            //the function of searching partner according to label
            onSearch={onSearch}                
            //the function of changing label of searching of partner name
            onChangeInput={handleChangeInput}   
            //the function of selecting ID partner
            onSelectUser={onSelectUser}         
            //the function of adding dialog
            onModalOk={onAddDialog}             
            //the function of composing message to partner
            onChangeTextArea={onChangeTextArea} 
            //the ID partner
            selectedUserId={selectedUserId}    
            //the list partners 
            users={users}                       
        />    
    )
}

export default connect(
    ({
        //user we receiving from ../../redux/reducers/user.js
        user 
    }) => ({
        //from user is being found user - user.data
        user: user.data 
    })
)(SidebarContainer);