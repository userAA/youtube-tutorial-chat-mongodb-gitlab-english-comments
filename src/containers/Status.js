import React from "react";
import { Status as StatusBase} from "../components"
import {connect} from "react-redux";

const Status = (props) => {
    const {
        //we receiving from ../../redux/reducers/dialogs with help connect
        dialogs,
        //dialog ID is being found from dialogs
        currentDialogId, 
        //user - authorized user from ../../redux/reducers/user with help connect
        user 
    } = props;

    //if there are no dialogs or dialog is not identified that status of partner is not being determined
    if (!dialogs.length || !currentDialogId) {
        return null;
    }

    //we defining needed dialog with ID currentDialogId from list dialogs
    const currentDialogObj = dialogs.filter(
        dialog => dialog._id === currentDialogId
    )[0];
    
    let partner = {};

    //we defining partner to whom we writing messages, the messages come from user with ID user._id
    if (currentDialogObj.author._id === user._id) {
        partner = currentDialogObj.partner; 
    } else {
        partner = currentDialogObj.author;
    }

    //partner.isOnline if partner during three minutes does not watch or answer on messages then partner.isOnline 
    //is being becomed false overwise partner.isOnline is being become true again
    return (
        <StatusBase  
            //status partner ( whether he reads or no sent to him messages)
            online={partner.isOnline}
            //full name of partner
            fullname={partner.fullname} 
        />
    );
}

export default connect(
    ({
        //dialogs we receiving from  ../../redux/reducers/dialogs.js
        dialogs, 
        //user we receiving from ../../redux/reducers/user.js
        user      
    }) => ({
        //from dialogs is being found dialogs - dialogs.items
        dialogs: dialogs.items,     
        //from dialogs is being found currentDialogId - dialogs.currentDialogId
        currentDialogId: dialogs.currentDialogId, 
        //from user is being found user - user.data
        user: user.data 
    })
)(Status);
