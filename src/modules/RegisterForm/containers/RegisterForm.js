import {withFormik} from 'formik';
import RegisterForm from "../components/RegisterForm";

import {userActions} from "../../../redux/actions";
import validateForm from "../../../utils/validate.js";

import store from "../../../redux/store";

export default withFormik({
    enableReinitialize: true,
    //template data according to user
    mapPropsToValues: () => ({
        email: '',
        fullname: '',
        password: '',
        password_2: ''
    }),
    //correctness of the entered data
    validate: values => {
        let errors = {};
        validateForm({isAuth: false, values, errors});
        return errors;
    },
    //we registering user
    handleSubmit: (values, {setSubmitting, props}) => {
        store
            .dispatch(userActions.fetchUserRegister(values))
            .then(({status}) => {
                if (status === 'success') {
                    setTimeout(() => {
                        props.history.push('/');    
                    }, 50);
                }
                setSubmitting(false);
            })
            .catch(() => {
                setSubmitting(false);
            })
            
    },
    displayName: "RegisterForm"
})(RegisterForm)
