import React from 'react';
import {Route} from "react-router-dom";

import {LoginForm,RegisterForm} from "../../modules";

import "./Auth.scss";

const Auth = () => {
    return (
        <section className="auth">
            <div className="auth__content">
                {/*router of starting up of page authorization */}
                <Route exact path="/signin" component={LoginForm}/>
                {/*router of starting up of page registration*/}
                <Route exact path="/signup" component={RegisterForm}/> 
            </div>
        </section>
    )
}

export default Auth;