import React, {useEffect} from 'react';
import {withRouter} from "react-router";
import {ChatInput, Messages, Status, Sidebar} from "../../containers";
import {connect} from "react-redux";

import "./Home.scss";

import {dialogsActions} from "../../redux/actions";

const Home = (props) => {
    const {
        //function setCurrentDialogId, is being got from ../../redux/actions/dialogsActions with help connect
        setCurrentDialogId, 
        //user - authorized user, is being got from ../../redux/reducers with help connect
        user                
    } = props;
    
    useEffect(() => {
        //the name of web-pages without http://localhost:3003
        const { pathname } = props.location;
        //we identificating of ID dialog
        const dialogId = pathname.split('/').pop();
        //we fixing ID of dialog
        setCurrentDialogId(dialogId);
        //if props.location.pathname or props.location or setCurrentDialogId is being changed that this hook starts up again
    }, [props.location.pathname,props.location,setCurrentDialogId]);

    return (
        <section className="home">
            <div className="chat">
                <Sidebar/>
                {user && (
                    <div className="chat__dialog">
                        <Status/> 
                        <Messages />
                        <div className="chat__dialog-input">
                            <ChatInput/>
                        </div>   
                    </div>
                )}
            </div>
        </section>
    )
}

export default withRouter(connect( ({user}) => (
        //from ../../redux/reducers/user.js is being found user 
        {user: user.data}), 
        //from dialogsActions is being found function setCurrentDialogId
        dialogsActions  
    )
(Home));