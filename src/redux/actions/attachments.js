import {filesApi} from "../../utils/api";

const Actions = {  
    //we receiving all files of message
    setAttachments: items => ({
        type: "ATTACHMENTS:SET_ITEMS",
        payload: items
    }),
    //we remove file from list files of message, sending to server
    //we also delete this file from the database
    removeAttachment: (file) => (dispatch) => {
        filesApi.removeByUid(file.uid).then(() => {
            dispatch({
                type: "ATTACHMENTS:REMOVE_ITEM",
                payload: file
            });
        })
    }
};

export default Actions;
