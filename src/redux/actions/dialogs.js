import {dialogsApi} from "../../utils/api";

const Actions = {
    //we receiving the list of all dialogs
    setDialogs: items => ({
        type: "DIALOGS:SET_ITEMS",
        payload: items
    }),
    //we setuping ID of current dislog id
    setCurrentDialogId: id => (dispatch) => {
        dispatch ({
            type: "DIALOGS:SET_CURRENT_DIALOG_ID",
            payload: id
        })
    },
    //we receiving information about all dialogs
    fetchDialogs: () => (dispatch) => {
        dialogsApi.getAll().then(({data}) => { 
            return ( 
               dispatch(Actions.setDialogs(data))
            );
        });
    }
}

export default Actions;