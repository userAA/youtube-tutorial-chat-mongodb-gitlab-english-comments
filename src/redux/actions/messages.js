import {filesApi} from "../../utils/api";
import {messagesApi} from "../../utils/api";

const Actions = {
    //we receiving all mesages of dialog
    setMessages: items => ({
        type: "MESSAGES:SET_ITEMS",
        payload: items
    }),
    //is being added last message into dialog of messages
    addMessage: (message) => (dispatch, getState) => {
        //getState full state from containers/Messages with help connect attachments dialogs user messages
        const {dialogs} = getState();
        const {currentDialogId} = dialogs;

        if (currentDialogId === message.dialog._id) {
            dispatch({
                type: "MESSAGES:ADD_MESSAGE",
                payload: message
            })
        }
    },
    //we adding last message into database on server
    fetchSendMessage: ({text, dialogId, attachments}) => () => {
        return messagesApi.send(text, dialogId, attachments);
    },
    //we uploading detector of uploading of all available dialog messages
    setIsLoading: bool => ({
        type: "MESSAGES:SET_IS_LOADING",
        payload: bool
    }),
    //we removing message according to ID
    removeMessageById: (item) => dispatch => {
        if (window.confirm("Do you really want to delete the message?")) 
        {
            //we removing files, related with removing message from database according to files
            if (item.attachments.length>0)
            {
                item.attachments.map(item => {
                    return (
                            filesApi.removeByUid(item._id).then(() => {
                            dispatch({
                                type: "ATTACHMENTS:REMOVE_ITEM",
                                payload: item
                            });
                        })
                    )
                })
            }

            //we removing from database itself message according ID
            messagesApi.removeById(item._id).then(() => {
                dispatch({
                    type: "MESSAGES:REMOVE_MESSAGE",
                    payload: item._id
                });
            })
        }
    },
    //we downloading messages from server according to ID of dialog dialogId
    fetchMessages: (dialogId) => dispatch => {
        //we including the detector of downloading messages
        dispatch(Actions.setIsLoading(true));
        //we receiving full information about messages in dialog
        messagesApi.getAllByDialogId(dialogId)
        .then(({data}) => {
            //we receiving messages
            dispatch(Actions.setMessages(data));
            //we turning off the detector of downloading messages
            dispatch(Actions.setIsLoading(false));
        })
        .catch(() => {
            //we turning off detector of downloading messages
            dispatch(Actions.setIsLoading(false));
        })
    }
}

export default Actions;