import {openNotification} from "../../utils/helpers";
import {userApi} from "../../utils/api";

const Actions = {
    //The request for receiving user data
    setUserData: data => ({
        type: "USER:SET_DATA",
        payload: data
    }),
    //the request for user authorization, there is it succesfull or no
    setIsAuth: bool => ({
        type: "USER:SET_IS_AUTH",
        payload: bool
    }),
    //the request for definition of user data
    fetchUserData: () => dispatch => {
        userApi.getMe().then(({data}) => {
            dispatch(Actions.setUserData(data));
        }).catch(err => {
            console.log(err);
            if (err.response.status === 403) {
                //error we considering that the user unauthorized
                dispatch(Actions.setIsAuth(false));
                //we removing his token
                delete window.localStorage.token;
            }
        })
    },
    //user authorization
    fetchUserLogin: postData => dispatch => {
        return userApi.signIn(postData).then(({data}) => {
            const {token} = data;
            //we outputing message about that user authorization is finished succesfully.
            openNotification({
                title: "Great!",
                text: "Authorization is successful.",
                type: "success"
            })

            //we receiving user token
            window.axios.defaults.headers.common["token"] = token;
            window.localStorage["token"] = token;
            //we defining user data
            dispatch(Actions.fetchUserData());
            //user is authorized
            dispatch(Actions.setIsAuth(true));
            return data;
        }).catch(({response}) => {
            if (response.status === 403) {
                openNotification({
                    title: "Authorization error",
                    text: "Invalid login or password",
                    type: "error"
                })
            }
        })
    },
    
    //user registration
    fetchUserRegister: postData => () => {
        return userApi.signUp(postData).then(({data}) => {
            return data;
        });
    }
};

export default Actions;
