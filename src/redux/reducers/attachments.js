//the storage of files
const initialState = {
    //the list of files for sending into message
    items: []                           
};
  
const attachments = (state = initialState, { type, payload }) => {
    switch (type) {
        //we receiving all files for sending into message
        case "ATTACHMENTS:SET_ITEMS":
            return {
                ...state,
                items: payload
            };
        //we removing file from files for sending into message
        case "ATTACHMENTS:REMOVE_ITEM":
            return {
                ...state,
                items: state.items.filter(item => item.uid !== payload.uid)
            };
        default:
            return state;
    }
};

export default attachments;