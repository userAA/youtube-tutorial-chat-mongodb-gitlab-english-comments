const initialState = {
    //list of dialogs
    items: [],                                                      
    //the ID of current dialog
    currentDialogId: window.location.pathname.split('dialog/')[1],  
    //the flag of uploading dialogs
    isLoading: false                                                
}

const dialogs = (state=initialState, {type,payload}) => {
    switch (type) {
        //we receiving the list of dialogs
        case "DIALOGS:SET_ITEMS":
            return {
                ...state,
                items: payload
            };
        //we receiving ID of current dialog
        case "DIALOGS:SET_CURRENT_DIALOG_ID":
            return {
                ...state,
                currentDialogId: payload
            };
        default:
        {
            return state;
        }
    }
}

export default dialogs;