const initialState = {
    //the list of all messages according to specified dialog
    items: [],              
    //the flag of uploading  messages according to specified dialog
    isLoading: false        
}

const messages = (state=initialState, {type,payload}) => {
    switch (type) {
        //we adding last message to available according to specified dialog
        case "MESSAGES:ADD_MESSAGE":
            return {
                ...state,
                items: [
                    ...state.items,
                    payload
                ]
            };
        //we receiving all messages according to available dialog
        case "MESSAGES:SET_ITEMS":
            return {
                ...state,
                items: payload,
                isLoading: false
            };
        //we removing message
        case 'MESSAGES:REMOVE_MESSAGE':
            return {
                ...state,
                items: state.items.filter(message => message._id !== payload),
            };
        //we fixing the state of uploading messages
        case "MESSAGES:SET_IS_LOADING":
            return {
                ...state,
                isLoading: payload
            };
        default:
            return state;
    }
}

export default messages;
