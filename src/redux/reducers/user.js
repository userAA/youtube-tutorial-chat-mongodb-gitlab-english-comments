const initialState = {
    //user data
    data: null,                       
    //user token
    token: window.localStorage.token, 
    //flag user
    isAuth: false                     
}

const user = (state=initialState, {type,payload}) => {
    switch (type) {
        //we receiving total information about authorized user
        case "USER:SET_DATA":
            return {
                ...state,
                data: payload,
                isAuth: true,
                token: window.localStorage.token
            };
        //we receiving information about whether is the user logged or no
        case "USER:SET_IS_AUTH":
            return {
                ...state,
                isAuth: payload
            };
        default:
            return state;
    }
}

export default user;