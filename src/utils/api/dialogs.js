import {axios} from '../../core';

const dialogsApi =  {
    //we receiving all dialogs
    getAll: () => {
        return (
            axios.get("/dialogs")
        )
    },
    //we creating dialog
    create: ({partner,text}) => {
        return (
            axios.post("/dialogs",{partner,text})
        )
    },
}

export default dialogsApi;
