import {axios} from '../../core';

const filesApi =  {
    //we uploading file  
    upload: (file) => {
        const formData = new FormData();
        formData.append("file",file);
        return (
            axios.post("/files",formData, {
                headers: {
                    "Content-Type": "multipert/form-data"
                }
            })
        )
    },
    //we removing message from database according to specified ID id
    removeByUid: (uid) => {
        return (
            axios.delete("/files?uid=" + uid)
        )
    },
}

export default filesApi;