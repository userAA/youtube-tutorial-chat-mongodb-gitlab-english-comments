import {axios} from '../../core';

const messagesApi =  {
    //we receiving all messages from specified dialog
    getAllByDialogId: (id) => {        
        return (
            axios.get("/messages?dialog=" + id)
        )
    },
    //we removing message from database according specified ID id
    removeById: (id) => {
        return (
            axios.delete("/messages?id=" + id)
        )
    },

    //we sending message into database of messages on server
    //text - text of sending message
    //dialogId - dialog ID in which user sends messages to partner
    //attachments - the list of identifiers of image files that is included in the message being sent
    send: (text,dialogId,attachments) => {
        return (
            axios.post("/messages", {
                text: text,
                dialog_id: dialogId,
                attachments
            })
        )
    }
}

export default messagesApi;