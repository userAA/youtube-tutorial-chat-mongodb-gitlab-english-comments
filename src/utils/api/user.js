import {axios} from "../../core";

const userApi = {    
    //user authorization
    signIn: (postData) => {
        return axios.post ("/user/signin",postData)
    },
    //user registration
    signUp: (postData) => {
        return axios.post ("/user/signup",postData)
    },
    //we receiving from server information about authorized user
    getMe: () => {
        return axios.get ("/user/me")
    },
    //search partner
    findUsers: (query) => {
        return axios.get ("/user/find?query=" + query)
    }
}

export default userApi;
