import {notification} from "antd";

//appearance of information window during three seconds
const openNotification = ({ text, type = 'info', title, duration = 3 }) =>
{
    return (
        notification[type]({
            message: title,
            description: text,
            duration,
        })
    )
}

export default openNotification;