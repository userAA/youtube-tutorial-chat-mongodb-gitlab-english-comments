//checking function of correction of filling fields at registration or authorization of user
const validateField = (key,touched,errors) => {
    if (touched[key]) {
        if (errors[key]) {
            return "error";
        } else {
            return "success";
        }
    } else {
        return "";
    }
}

export default validateField;