const validateForm = ({isAuth,values,errors}) => {
    const rules = {
        //template of email
        email: value => {
            if (!value) {
                errors.email = "Enter E-Mail";
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
            ) {
                errors.email = "Incorrect E-Mail";
            }
        },
        //template of password
        password: value => {
            if (!value) {
                errors.password = "Enter password";
            } else if (!isAuth && !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/i.test(value)) {
                errors.password = "The password is too easy";
            }
        },
        //checking that passwords must match
        password_2: value => {
            if ((!value || !values.password) || (!isAuth && value !== values.password)) {
                errors.password_2 = "Passwords don't match"
            } 
        },
        //need to specify the fullname when registering
        fullname: value => {
            if (!isAuth && !value) {
                errors.fullname = "Enter your first and last name";
            }
        }
    };

    return  Object.keys(values).forEach(key => rules[key] && rules[key](values[key]));
}

export default validateForm;